// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_HudBase.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UW_HudBase : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "W_HudBase")
		void OpenTab(int Tab);


};
