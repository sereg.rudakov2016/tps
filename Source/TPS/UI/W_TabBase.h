// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "W_TabBase.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UW_TabBase : public UUserWidget
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "W_TabBase")
		void OnCloseTab();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "W_TabBase")
		void PreOpenTab();
};
