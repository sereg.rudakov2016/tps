// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "UObject/Interface.h"
#include "IMenuPCTab.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIMenuPCTab : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API IIMenuPCTab
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = ITab_PC_Base)
		void ITabMenuOpen(enum ETypeMenuTab Tab);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = ITab_PC_Base)
		void ITabGameMenuOpen(enum ETypeMenuGameTab Tab);
};
