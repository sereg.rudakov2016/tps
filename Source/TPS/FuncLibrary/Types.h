// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Styling/SlateBrush.h"
#include "Types.generated.h"




UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim_State"),
	Walk_State UMETA(DisplayName = "Walk_State"),
	Run_State UMETA(DisplayName = "Run_State"),
};

UENUM(BlueprintType)
enum class ETypeWarriorClass : uint8
{
	Archer        UMETA(DisplayName = "Archer"),
	WarriorStaff  UMETA(DisplayName = "WarriorStaff"),
	WarriorShield     UMETA(DisplayName = "WarriorShield")

};

UENUM(BlueprintType)
enum class ETypeItem : uint8
{
	Potion        UMETA(DisplayName = "Potion"),
	Weapon        UMETA(DisplayName = "Weapon"),
	Ammo          UMETA(DisplayName = "Ammo"),
	Armor         UMETA(DisplayName = "Armor")

};

UENUM(BlueprintType)
enum class ETypeMenuTab : uint8
{
	MenuLoad        UMETA(DisplayName = "MenuLoad"),
	MenuMain        UMETA(DisplayName = "MenuMain"),
	MenuSelectHero  UMETA(DisplayName = "MenuSelectHero"),
	MenuSettings    UMETA(DisplayName = "MenuSettings"),
	MenuMissions    UMETA(DisplayName = "MenuMissions"),
	MenuLoadGame    UMETA(DisplayName = "MenuLoadGame"),
	MenuRewardGame  UMETA(DisplayName = "MenuRewardGame")
};

UENUM(BlueprintType)
enum class ETypeMenuGameTab : uint8
{
	GameLoad         UMETA(DisplayName = "MenuLoad"),
	GameWorld        UMETA(DisplayName = "GameWorld"),
	GameMenu         UMETA(DisplayName = "MenuMain"),
	GameSettings     UMETA(DisplayName = "GameSettings"),
	GameDeathPlayer  UMETA(DisplayName = "GameDeathPlayer"),
	GameGameEnd      UMETA(DisplayName = "GameGameEnd")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 600.0f;
};

USTRUCT(BlueprintType)
struct FStrWeaponInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText WeaponName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USkeletalMesh* SK_Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMesh* SM_Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectileDefaultBase> WeaponProjectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ETypeWarriorClass TypeWarriorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName WeaponSocket;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isNeedAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SpeedAttack = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageWeapon = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AFXHITBase> FXHit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FDataTableRowHandle BuffFX;
};

USTRUCT(BlueprintType)
struct FStrAmmoInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText AmmoName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AmmoSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CountShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SpreadShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AExplosionBase> ExplosionAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UParticleSystem* BulletFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMesh* BulletMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ReloadTime = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageAmmo = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FDataTableRowHandle BuffFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FXProperty")
		FDataTableRowHandle HitFXOwerlay;
};


USTRUCT(BlueprintType)
struct FStrItemInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FSlateBrush ItemIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FDataTableRowHandle ItemContent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		ETypeItem TypeItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		ETypeWarriorClass TypeClassPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		bool isNotRemove;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		class UStaticMesh* WorldMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		float WorldMeshSize = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FDataTableRowHandle BuffFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		int ValueStat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemProperty")
		FDataTableRowHandle PickableFX;
};

USTRUCT(BlueprintType)
struct FStrBuffInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffProperty")
		FText BuffName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffProperty")
		FSlateBrush BuffIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffProperty")
		int BuffTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffProperty")
		int BuffDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffProperty")
		bool isStacableBuff = false;
};


USTRUCT(BlueprintType)
struct FStrFXHit : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:



	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UParticleSystem* PTurnFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UNiagaraSystem* NTurnFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USoundCue* FXSound;
};


UCLASS()
class TPS_API UTypes: public UBlueprintFunctionLibrary
{

	GENERATED_BODY()
};
