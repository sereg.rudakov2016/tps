// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "ProjectileDefaultBase.h"
#include "DrawDebugHelpers.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::AttackWeapon(int & CountAmmo)
{
	CountAmmo = 0;
	if (CurrWeapon.WeaponProjectile)
	{
		FVector Loc = FVector(0.0f, 0.0f, 0.0f);
		if (SkeletalMeshWeapon) { Loc = SkeletalMeshWeapon->GetSocketLocation("Projectile"); }
		if (StaticMeshWeapon) { Loc = StaticMeshWeapon->GetSocketLocation("Projectile"); }

		float RotZ = GetOwner()->GetActorRotation().Yaw - CurrAmmo.CountShot * CurrAmmo.SpreadShot;

		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this->GetOwner();
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		for (int i = 0; i < CurrAmmo.CountShot; i++)
		{
			RotZ = RotZ + CurrAmmo.SpreadShot * i;
			AProjectileDefaultBase* LocProject = GetWorld()->SpawnActor<AProjectileDefaultBase>(CurrWeapon.WeaponProjectile, Loc, FRotator(0.0f, RotZ, 0.0f), SpawnParams);
			LocProject->StartProjectile(CurrAmmo);
			LocProject->Damage = CurrAmmo.DamageAmmo + CurrWeapon.DamageWeapon;
			//DrawDebugLine(GetWorld(), Loc, Loc + LocProject->GetActorForwardVector()*700.f, FColor::Red, false, 1.f, (uint8)'\000', 2.0f);
			CountAmmo = CountAmmo + 1;

		}
	}
}

void AWeaponBase::SpawnWeapon_Implementation(ETypeWarriorClass TypeClassWarrior)
{

}

