// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ComponentInventoryBase.h"

// Sets default values for this component's properties
UComponentInventoryBase::UComponentInventoryBase()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UComponentInventoryBase::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UComponentInventoryBase::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UComponentInventoryBase::AddNewItem(FName RowItem, int CountItem, ETypeItem GetTypeItem)
{
	int CurrCount = 0;
	switch (GetTypeItem)
	{
	case ETypeItem::Potion:
		if(PotionItems.Contains(RowItem))
		{
			CurrCount = PotionItems[RowItem];
			PotionItems.Remove(RowItem);
			PotionItems.Add(RowItem, CurrCount + CountItem);
		}
		else
		{
			PotionItems.Add(RowItem, CountItem);
		}
		break;
	case ETypeItem::Weapon:
		if (WeaponItems.Contains(RowItem))
		{
			//CurrCount = WeaponItems[RowItem];
			//WeaponItems.Remove(RowItem);
			//WeaponItems.Add(RowItem, CurrCount + CountItem);
		}
		else
		{
			WeaponItems.Add(RowItem, CountItem);
			WeaponItemsKey.Add(RowItem);
		}
		
		break;
	case ETypeItem::Ammo:
		if (AmmoItems.Contains(RowItem))
		{
			CurrCount = AmmoItems[RowItem];
			AmmoItems.Remove(RowItem);
			AmmoItems.Add(RowItem, CurrCount + CountItem);
		}
		else
		{
			AmmoItems.Add(RowItem, CountItem);
			AmmoItemsKey.Add(RowItem);
		}
		
		break;
	case ETypeItem::Armor:
		if (ArmorItems.Contains(RowItem))
		{
			CurrCount = ArmorItems[RowItem];
			ArmorItems.Remove(RowItem);
			ArmorItems.Add(RowItem, CurrCount + CountItem);
		}
		else
		{
			ArmorItems.Add(RowItem, CountItem);
		}
		break;
	default:
		break;
	}

}
//
bool UComponentInventoryBase::RemoveItem(FName RowItem, int Count, ETypeItem TypeItem)
{
	bool LRemove = true;
	switch (TypeItem)
	{
	case ETypeItem::Potion:
		if (PotionItems.Contains(RowItem))
		{
			int CurrCount = PotionItems[RowItem];
			PotionItems.Remove(RowItem);
			if (CurrCount - Count > 0)
			{
				PotionItems.Add(RowItem, CurrCount - Count);
				LRemove = false;
			}
		}

		break;
	case ETypeItem::Weapon:
		if (WeaponItems.Contains(RowItem))
		{
			int CurrCount = WeaponItems[RowItem];
			WeaponItems.Remove(RowItem);
			if (CurrCount - Count > 0)
			{
				WeaponItems.Add(RowItem, CurrCount - Count);
				LRemove = false;
			}
			else
			{
				WeaponItemsKey.Remove(RowItem);
			}
		}
		WeaponItems.GenerateKeyArray(WeaponItemsKey);

		break;
	case ETypeItem::Ammo:
		if (AmmoItems.Contains(RowItem))
		{
			int CurrCount = AmmoItems[RowItem];
			AmmoItems.Remove(RowItem);
			if (CurrCount - Count > 0)
			{
				AmmoItems.Add(RowItem, CurrCount - Count);
				LRemove = false;
			}
			else
			{
				AmmoItemsKey.Remove(RowItem);
			}
		}

		break;
	case ETypeItem::Armor:
		if (ArmorItems.Contains(RowItem))
		{
			int CurrCount = ArmorItems[RowItem];
			ArmorItems.Remove(RowItem);
			if (CurrCount - Count > 0)
			{
				ArmorItems.Add(RowItem, CurrCount - Count);
				LRemove = false;
			}
		}

		break;
	default:
		break;
	}
	return LRemove;
}

TMap<FName, int> UComponentInventoryBase::ChangedItems(TMap<FName, int> GetMap, FName Newitem, int CountNewItem, bool isRemove)
{
	//int LocIDItem = -1;
	//TMap<FName, int> NewMap;
	//TArray<FName> LocItems;
	//TArray<int> LocCountItems;
	//GetMap.GenerateKeyArray(LocItems);
	//GetMap.GenerateValueArray(LocCountItems);

	//if(isRemove)
	//{
	//	
	//	LocIDItem = LocItems.Remove(Newitem);
	//	if (LocIDItem >= 0) { LocItems.RemoveAt(LocIDItem); }
	//}
	//else
	//{
	//	LocIDItem.Add(Newitem);
	//}

	//if (LocItems.Num() > 1)
	//{
	//	//WeaponItems.Empty();
	//	//FName LItem = LocItems[0];


	//	for (int i = 0; i < LocItems.Num(); i++)
	//	{
	//		if (i != 0)
	//			WeaponItems.Add(LocItems[i], 1);
	//	}

	//	WeaponItems.Add(LItem, 1);
	//	return true;
	//}
	return GetMap;

}


bool UComponentInventoryBase::ChangedNextWeapon()
{
	TArray<FName> LocItems;

	WeaponItems.GenerateKeyArray(LocItems);

	if (LocItems.Num() > 1)
	{
		WeaponItems.Empty();
		FName LItem = LocItems[0];


		for (int i = 0; i < LocItems.Num(); i++)
		{
			if (i != 0)
				WeaponItems.Add(LocItems[i], 1);
		}

		WeaponItems.Add(LItem, 1);
		return true;
	}
	return false;

}

bool UComponentInventoryBase::ChangedNextAmmo()
{

	//TArray<FName> LocItems;
	//TArray<int> LCounts;
	//AmmoItems.GenerateKeyArray(LocItems);
	//if (LocItems.Num() > 1)
	//{
	//	AmmoItems.GenerateValueArray(LCounts);
	//	AmmoItems.Empty();
	//	FName LItem = LocItems[0];
	//	int LCount = LCounts[0];

	//	for (int i = 0; i < LocItems.Num(); i++)
	//	{
	//		if (i != 0)
	//			AmmoItems.Add(LocItems[i], LCounts[i]);
	//	}

	//	AmmoItems.Add(LItem, LCount);
	//	return true;
	//}
	if (AmmoItemsKey.Num() > 1)
	{
		FName LocAmmo = AmmoItemsKey[0];
		AmmoItemsKey.RemoveAt(0);
		AmmoItemsKey.Add(LocAmmo);
		return true;
	}
	return true;
}

FName UComponentInventoryBase::GetCurrWeapon()
{
	TArray<FName> LocItems;
	WeaponItems.GenerateKeyArray(LocItems);
	if(LocItems.Num() > 0){ return LocItems[0]; }
	return "None";
}

FName UComponentInventoryBase::GetCurrAmmo()
{
	//TArray<FName> LocItems;
	//AmmoItems.GenerateKeyArray(LocItems);
	//if (LocItems.Num() > 0) { return LocItems[LocItems.Num()-1]; }
	//return "None";
	if(AmmoItemsKey.Num()>0)
	{ 
		return AmmoItemsKey[0]; 
	}
	else
	{
	}
	return "None";
}
