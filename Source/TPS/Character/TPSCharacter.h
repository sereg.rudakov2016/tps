// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "TPSCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoReloadStart, float, Timer);



UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	UPROPERTY(BlueprintAssignable)
	FOnAmmoReloadStart OnAmmoReloadStart;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		//class UComponentHealthPlayerBase* PlayerHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;



public:
	UPROPERTY(BlueprintReadWrite, Category = "Movement")
		FRotator ResultRotationCursor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property")
		ETypeWarriorClass TypeClassWarrior;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property")
		TSubclassOf<class AWeaponBase> CurrWeaponClass;

//
	UPROPERTY(BlueprintReadWrite, Category = "Movement")
		bool isSprint;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
		bool isCanActions = true;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		bool isAttackPressed;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		bool isAttackProcess;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		bool isReload;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		bool isActions;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		class AWeaponBase* CurrWeapon;

	UPROPERTY(BlueprintReadWrite, Category = "TPSCharacter_Attack")
		FTimerHandle ReloadTimer;

	UFUNCTION()
		void InputAxisX(float value);

	UFUNCTION()
		void InputAxisY(float value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
		bool GetCanAttack();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TPSCharacter_Attack")
		void StartAttack();

	UFUNCTION(BlueprintCallable)
		void Attack();

	UFUNCTION(BlueprintCallable)
		bool ReloadAmmoStart();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "TPSCharacter_Attack")
		void ReloadAmmoEnd();
};

