// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/WorldItemBase.h"
#include "Components/SphereComponent.h"
#include "Types.h"
// Sets default values
AWorldItemBase::AWorldItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	RootComponent = ItemCollisionSphere;

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	ItemMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWorldItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWorldItemBase::SpawnItem(FDataTableRowHandle DT, int SpCount)
{
	if (DT.DataTable)
	{
		if (FStrItemInfo* data = DT.DataTable->FindRow<FStrItemInfo>(DT.RowName, ""))
		{
			DTItem = DT;
			ItemMesh->SetStaticMesh(data->WorldMesh);
			ItemMesh->SetWorldScale3D(FVector(data->WorldMeshSize, data->WorldMeshSize, data->WorldMeshSize));
			Count = SpCount;
		}
	}
}

