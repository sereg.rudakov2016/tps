// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "ProjectileDefaultBase.generated.h"

UCLASS()
class TPS_API AProjectileDefaultBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileDefaultBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
		struct FStrAmmoInfo Ammo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage = 1.0f;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void StartProjectile(struct FStrAmmoInfo SetAmmo);

};
