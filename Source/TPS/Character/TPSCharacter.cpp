// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "ProjectileDefaultBase.h"
#include "FXHITBase.h"
#include "ExplosionBase.h"
#include "WeaponBase.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Characters/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//PlayerHealthComponent = CreateDefaultSubobject<UComponentHealthPlayerBase>(TEXT("HealthComponent"));
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	if (!isActions)
	{
		MovementTick(DeltaSeconds);
		if (GetCanAttack()) { StartAttack(); }
	}
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
}

void ATPSCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void ATPSCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATPSCharacter::MovementTick(float DeltaTime)
{

		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController && !isSprint && isCanActions)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			FRotator Rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
			ResultRotationCursor = Rot;
			SetActorRotation(FQuat(FRotator(0.0f, Rot.Yaw, 0.0f)));
		}

}


void ATPSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

bool ATPSCharacter::GetCanAttack()
{
	
	if (isActions) { return false; }
	if(!CurrWeapon){ return false; }
	if (!isAttackPressed) { return false; }
	if (isAttackProcess) { return false; }
	if(isReload) { return false; }
	if(CurrWeapon->CurrWeapon.isNeedAmmo) 
	{ 
		if(CurrWeapon->CurrAmmoCount <= 0)
		return false;
	}

	return true;
}


void ATPSCharacter::StartAttack_Implementation()
{
	isAttackProcess = true;
	if (CurrWeapon->CurrWeapon.isNeedAmmo) { CurrWeapon->CurrAmmoCount--;	}
		


}

void ATPSCharacter::Attack()
{
	//if(CurrWeapon.WeaponProjectile)
	//{
	//	FVector Loc = FVector(0.0f, 0.0f, 0.0f);
	//	if (CurSKMesh) { Loc = CurSKMesh->GetSocketLocation("Projectile"); }
	//	if (CurSMMesh) { Loc = CurSMMesh->GetSocketLocation("Projectile"); }

	//	float RotZ = GetActorRotation().Yaw - CurrAmmo.CountShot * CurrAmmo.SpreadShot;
	//	FRotator Rot = GetActorRotation();
	//	FActorSpawnParameters SpawnParams;
	//	SpawnParams.Owner = this;
	//	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	//	for (int i = 0; i < CurrAmmo.CountShot; i++)
	//	{
	//		RotZ = RotZ + CurrAmmo.SpreadShot * i;
	//		AProjectileDefaultBase* LocProject = GetWorld()->SpawnActor<AProjectileDefaultBase>(CurrWeapon.WeaponProjectile, Loc, FRotator(0.0f, RotZ, 0.0f), SpawnParams);
	//		LocProject->StartProjectile(CurrAmmo);

	//	}
	//}
}

bool ATPSCharacter::ReloadAmmoStart()
{
	if (!isReload)
	{

		isReload = true;
		GetWorld()->GetTimerManager().SetTimer(ReloadTimer, this, &ATPSCharacter::ReloadAmmoEnd, CurrWeapon->CurrAmmo.ReloadTime);
		OnAmmoReloadStart.Broadcast(CurrWeapon->CurrAmmo.ReloadTime);
		return true;
	}
	return false;
}

void ATPSCharacter::ReloadAmmoEnd_Implementation()
{
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
	isReload = false;
}