// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ComponentHealthBase.h"
#include "BuffActorBase.h"
// Sets default values for this component's properties
UComponentHealthBase::UComponentHealthBase()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UComponentHealthBase::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UComponentHealthBase::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UComponentHealthBase::GetCurrentHealth()
{
	return Health;;
}

void UComponentHealthBase::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UComponentHealthBase::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;
	

	if (ChangeValue < 0 && Armor > 0)
	{
		Armor = Armor + ChangeValue;
		if (Armor < 0)
		{
			ChangeValue = ChangeValue - Armor;
			Armor = 0.0f;
		}
		else
		{
			ChangeValue = 0;
		}
	}
	Health += ChangeValue;

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			isDeath = true;
			OnDead.Broadcast();
		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue, Armor);
}

float UComponentHealthBase::GetCurrentArmor()
{
	return Armor;
}

bool UComponentHealthBase::CheckStackableBuff(FName AddedBuf)
{
	//if(AddedBuf->SBuffInfo.isStacableBuff)
	//{
		return BuffsCurr.Contains(AddedBuf);
	//}

	

	return false;
}

bool UComponentHealthBase::CanAddedBuff(FName AddedBuf)
{
	if (CheckStackableBuff(AddedBuf)) { return false; }
	if(AddedBuf == "None"){ return false; }
	return true;
}

void UComponentHealthBase::RemoveBuff(FName RemovedBuf)
{
	BuffsCurr.Remove(RemovedBuf);
	OnChangedBuff.Broadcast();
}

void UComponentHealthBase::AddBuff(FName AddBuf)
{
	if(CanAddedBuff(AddBuf))
	{
		if (BuffInfoTable && BuffClass)
		{
			if (FStrBuffInfo* data = BuffInfoTable->FindRow<FStrBuffInfo>(AddBuf, ""))
			{
				FActorSpawnParameters Params;
				Params.Owner = GetOwner();
				ABuffActorBase* Buff = GetWorld()->SpawnActor<ABuffActorBase>(BuffClass, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation(), Params);
				Buff->AttachToActor(GetOwner(), FAttachmentTransformRules::SnapToTargetIncludingScale);
				BuffsCurr.Add(AddBuf);
				Buff->SpawnBuf(*data, AddBuf);
				OnChangedBuff.Broadcast();
			}
		}
	}

}

