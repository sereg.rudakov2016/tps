// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "ComponentHealthBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthChange, float, Health, float, Damage, float, Armor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangedBuff);

UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UComponentHealthBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UComponentHealthBase();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnChangedBuff OnChangedBuff;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float Health = 100.0f;

	float Armor = 0.0f;
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff")
		UDataTable* BuffInfoTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buff")
		TSubclassOf<class ABuffActorBase> BuffClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CoefDamage = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		bool isDeath = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buffs")
		TArray<FName> BuffsCurr;

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		float GetCurrentHealth();

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		void SetCurrentHealth(float NewHealth);

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		virtual void ChangeHealthValue(float ChangeValue);

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		float GetCurrentArmor();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Buffs")
		bool CheckStackableBuff(FName AddedBuf);

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		bool CanAddedBuff(FName AddedBuf);

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		void RemoveBuff(FName RemovedBuf);

	UFUNCTION(BlueprintCallable, Category = "Buffs")
		void AddBuff(FName AddBuf);
};
