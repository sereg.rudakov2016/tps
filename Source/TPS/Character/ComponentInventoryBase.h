// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Types.h"
#include "ComponentInventoryBase.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UComponentInventoryBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UComponentInventoryBase();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TSubclassOf<class AWorldItemBase> WorldItem;


	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TMap<FName, int> PotionItems;

	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TMap<FName, int> ArmorItems;

	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TMap<FName, int> WeaponItems;

	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TMap<FName, int> AmmoItems;


	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TArray<FName> AmmoItemsKey;

	UPROPERTY(BlueprintReadWrite, Category = "ComponentInventoryBase")
		TArray<FName> WeaponItemsKey;

	UFUNCTION(BlueprintCallable, Category = "ComponentInventoryBase")
		void AddNewItem(FName RowItem, int CountItem, ETypeItem GetTypeItem);

	UFUNCTION(BlueprintCallable, Category = "ComponentInventoryBase")
		bool RemoveItem(FName RowItem, int Count, ETypeItem TypeItem);

	UFUNCTION(BlueprintCallable, Category = "ComponentInventoryBase")
		bool ChangedNextWeapon();

	UFUNCTION(BlueprintCallable, Category = "ComponentInventoryBase")
		bool ChangedNextAmmo();

	UFUNCTION(BlueprintCallable, Category = "ComponentInventoryBase")
		TMap<FName, int> ChangedItems(TMap<FName, int> GetMap, FName Newitem, int CountNewItem, bool isRemove);



	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ComponentInventoryBase")
		FName GetCurrWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ComponentInventoryBase")
		FName GetCurrAmmo();
};
