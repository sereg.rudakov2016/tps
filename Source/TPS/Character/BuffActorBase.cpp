// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BuffActorBase.h"

// Sets default values
ABuffActorBase::ABuffActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuffActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuffActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABuffActorBase::SpawnBuf_Implementation(FStrBuffInfo SetBuf, FName RowBuff)
{

}

