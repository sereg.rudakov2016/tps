// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/ComponentHealthBase.h"
#include "ComponentHealthPlayerBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UComponentHealthPlayerBase : public UComponentHealthBase
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Health")
		void AddArmor(float NewArmor);
};
