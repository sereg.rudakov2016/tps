// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "WeaponBase.generated.h"



UCLASS()
class TPS_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property")
		int CurrAmmoCount = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property")
		FStrWeaponInfo CurrWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property")
		FStrAmmoInfo CurrAmmo;

	UFUNCTION(BlueprintCallable)
		void AttackWeapon(int& CountAmmo);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void SpawnWeapon(ETypeWarriorClass TypeClassWarrior);


};
