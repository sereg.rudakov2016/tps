// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ProjectileDefaultBase.h"

// Sets default values
AProjectileDefaultBase::AProjectileDefaultBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AProjectileDefaultBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileDefaultBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefaultBase::StartProjectile_Implementation(FStrAmmoInfo SetAmmo)
{
}

