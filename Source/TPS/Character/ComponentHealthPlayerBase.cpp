// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ComponentHealthPlayerBase.h"

void UComponentHealthPlayerBase::AddArmor(float NewArmor)
{
	Armor = Armor + NewArmor;
	if (Armor > 100) { Armor = 100; }
	OnHealthChange.Broadcast(Health, NewArmor, Armor);
}