// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPSGameInstanceBase.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UTPSGameInstanceBase : public UGameInstance
{
	GENERATED_BODY()

public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " TPSGameInstanceBase ")
		UDataTable* WeaponInfoTable = nullptr;



	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FStrWeaponInfo& OutInfo);
};
