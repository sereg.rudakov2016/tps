// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/PlayerSettingsBase.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameUserSettings.h"
void UPlayerSettingsBase::InitDASettings(UPlayerSettingsDataAsset* SettingsDatAsset)
{
	
	SettingsDataAsset = SettingsDatAsset;
	if (SettingsDataAsset)
	{
		if(LoadSettings())
		{
			ApplySettings();
		}
		else
		{
			SetDefaultSettings();
		}
	}
}

void UPlayerSettingsBase::ChangeSoundMasterVolume(float value)
{
	if (SettingsDataAsset)
	{

		if (SettingsDataAsset->MixMaster && SettingsDataAsset->ClassMaster)
		{

			UGameplayStatics::SetSoundMixClassOverride(this, SettingsDataAsset->MixMaster, SettingsDataAsset->ClassMaster, value);
			UGameplayStatics::PushSoundMixModifier(this, SettingsDataAsset->MixMaster);
			CurrSoundMaster = value;
		}
	}
}

void UPlayerSettingsBase::ChangeSoundMusicVolume(float value)
{

	if (SettingsDataAsset)
	{
		
		if (SettingsDataAsset->MixMusic && SettingsDataAsset->ClassMusic)
		{

			UGameplayStatics::SetSoundMixClassOverride(this, SettingsDataAsset->MixMusic, SettingsDataAsset->ClassMusic, value);
			UGameplayStatics::PushSoundMixModifier(this, SettingsDataAsset->MixMusic);
			CurrSoundMusic = value;
		}
	}
}

void UPlayerSettingsBase::SaveSettings()
{
	UPlayerSettingsSaveObject* SavedObject = Cast<UPlayerSettingsSaveObject>(UGameplayStatics::CreateSaveGameObject(UPlayerSettingsSaveObject::StaticClass()));
	if (SavedObject)
	{
		SavedObject->SaveMasterSoundVolume = CurrSoundMaster;
		SavedObject->SaveMusicSoundVolume = CurrSoundMusic;

		SavedObject->SaveScreenResolutionX = BaseScreenResolutionX;
		SavedObject->SaveScreenResolutionY = BaseScreenResolutionY;
		SavedObject->SaveScreenMode = BaseScreenMode;
		SavedObject->SaveResolutionScale = BaseResolutionScale;
		SavedObject->SaveMaterialQScale = BaseMaterialQScale;
		SavedObject->SaveAAQuality = BaseAAQuality;
		SavedObject->SaveViewDistance = BaseViewDistance;
		SavedObject->SaveShadowQuality = BaseShadowQuality;
		SavedObject->SaveFoliageQuality = BaseFoliageQuality;
		SavedObject->SaveTexturesQuality = BaseTexturesQuality;
		SavedObject->SaveEffectsQuality = BaseEffectsQuality;
		SavedObject->SavePPQuality = BasePPQuality;
		SavedObject->bSaveVSync = bBaseVSync;
		UGameplayStatics::SaveGameToSlot(SavedObject, "PlayerSettings", 0);
	}
}

bool UPlayerSettingsBase::LoadSettings()
{
	UPlayerSettingsSaveObject* SavedObject = Cast<UPlayerSettingsSaveObject>(UGameplayStatics::CreateSaveGameObject(UPlayerSettingsSaveObject::StaticClass()));
	if (SavedObject)
	{

		SavedObject = Cast<UPlayerSettingsSaveObject>(UGameplayStatics::LoadGameFromSlot("PlayerSettings", 0));
		if (SavedObject)
		{
			CurrSoundMaster = SavedObject->SaveMasterSoundVolume;
			CurrSoundMusic = SavedObject->SaveMusicSoundVolume;

			BaseScreenResolutionX = SavedObject->SaveScreenResolutionX;
			BaseScreenResolutionY = SavedObject->SaveScreenResolutionY;
			BaseScreenMode = SavedObject->SaveScreenMode;
			BaseResolutionScale = SavedObject->SaveResolutionScale;
			BaseMaterialQScale = SavedObject->SaveMaterialQScale;
			BaseAAQuality = SavedObject->SaveAAQuality;
			BaseViewDistance = SavedObject->SaveViewDistance;
			BaseShadowQuality = SavedObject->SaveShadowQuality;
			BaseFoliageQuality = SavedObject->SaveFoliageQuality;
			BaseTexturesQuality = SavedObject->SaveTexturesQuality;
			BaseEffectsQuality = SavedObject->SaveEffectsQuality;
			BasePPQuality = SavedObject->SavePPQuality;
			bBaseVSync = SavedObject->bSaveVSync;
			return true;
		}


		return false;
	}

	return false;
}

void UPlayerSettingsBase::ApplySettings()
{
	ChangeSoundMasterVolume(CurrSoundMaster);
	ChangeSoundMusicVolume(CurrSoundMusic);

	static IConsoleVariable* MaterialQualityLevelVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.MaterialQualityLevel"));
	MaterialQualityLevelVar->Set(BaseMaterialQScale);

	GEngine->GetGameUserSettings()->SetResolutionScaleValueEx(BaseResolutionScale);
	GEngine->GetGameUserSettings()->SetAntiAliasingQuality(BaseAAQuality);
	GEngine->GetGameUserSettings()->SetViewDistanceQuality(BaseViewDistance);
	GEngine->GetGameUserSettings()->SetShadowQuality(BaseShadowQuality);
	GEngine->GetGameUserSettings()->SetFoliageQuality(BaseFoliageQuality);
	GEngine->GetGameUserSettings()->SetTextureQuality(BaseTexturesQuality);
	GEngine->GetGameUserSettings()->SetVisualEffectQuality(BaseEffectsQuality);
	GEngine->GetGameUserSettings()->SetVSyncEnabled(bBaseVSync);
	GEngine->GetGameUserSettings()->SetPostProcessingQuality(BasePPQuality);

	FIntPoint Resolution = FIntPoint(BaseScreenResolutionX, BaseScreenResolutionY);

	GEngine->GetGameUserSettings()->SetScreenResolution(Resolution);
	GEngine->GetGameUserSettings()->SetFullscreenMode(static_cast<EWindowMode::Type>(BaseScreenMode));

	GEngine->GetGameUserSettings()->ApplySettings(false);

}

void UPlayerSettingsBase::SetDefaultSettings()
{
	CurrSoundMusic = 1.0;
	CurrSoundMaster = 1.0;

	BaseMaterialQScale = 1;
	BaseResolutionScale = 100;
	BaseAAQuality = 3;
	BaseViewDistance = 3;
	BaseShadowQuality = 3;
	BaseFoliageQuality = 3;
	BaseTexturesQuality = 3;
	BaseEffectsQuality = 3;
	BasePPQuality = 3;
	bBaseVSync = true;

	BaseScreenResolutionX = GEngine->GetGameUserSettings()->GetDesktopResolution().X;
	BaseScreenResolutionY = GEngine->GetGameUserSettings()->GetDesktopResolution().Y;

	BaseScreenMode = 0;

	ApplySettings();

}

void UPlayerSettingsBase::ChangeVideoResolution(int ResolutionX, int ResolutionY)
{
	BaseScreenResolutionX = ResolutionX;
	BaseScreenResolutionY = ResolutionY;
}

void UPlayerSettingsBase::ChangeVideoScreenMode(int32 ScreenMode)
{
	BaseScreenMode = ScreenMode;
}

void UPlayerSettingsBase::ChangeVideoResolutionScale(int32 ResolutionScale)
{
	BaseResolutionScale = ResolutionScale;
}

void UPlayerSettingsBase::ChangeMaterialQScale(int32 MaterialScale)
{
	BaseMaterialQScale = MaterialScale;
}

void UPlayerSettingsBase::ChangeVideoAntiAliasing(int32 AAQuality)
{
	BaseAAQuality = AAQuality;
}

void UPlayerSettingsBase::ChangeViewDistance(int32 ViewDistance)
{
	BaseViewDistance = ViewDistance;
}

void UPlayerSettingsBase::ChangeShadowQuality(int32 ShadowQuality)
{
	BaseShadowQuality = ShadowQuality;
}

void UPlayerSettingsBase::ChangeFoliageQualitye(int32 FoliageQuality)
{
	BaseFoliageQuality = FoliageQuality;
}

void UPlayerSettingsBase::ChangeTexturesQuality(int32 TexturesQuality)
{
	BaseTexturesQuality = TexturesQuality;
}

void UPlayerSettingsBase::ChangeEffectsQuality(int32 EffectsQuality)
{
	BaseEffectsQuality = EffectsQuality;
}

void UPlayerSettingsBase::ChangeVSync(bool VSync)
{
	bBaseVSync = VSync;
}
