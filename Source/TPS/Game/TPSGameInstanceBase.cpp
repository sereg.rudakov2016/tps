// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TPSGameInstanceBase.h"

bool UTPSGameInstanceBase::GetWeaponInfoByName(FName NameWeapon, FStrWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FStrWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FStrWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;

}
