// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Types.h"
#include "GameFramework/SaveGame.h"
#include "Engine/DataAsset.h"
#include "PlayerSettingsBase.generated.h"


UCLASS()
class TPS_API UPlayerSettingsSaveObject : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY()
		float SaveMasterSoundVolume;

	UPROPERTY()
		float SaveMusicSoundVolume;

	UPROPERTY()
		float SaveVoiseSoundVolume;

	UPROPERTY()
		float SaveAmbientSoundVolume;

	UPROPERTY()
		float SaveUISoundVolume;

	UPROPERTY()
		float SaveFXSoundVolume;

	UPROPERTY()
		int SaveScreenResolutionX;

	UPROPERTY()
		int SaveScreenResolutionY;

	UPROPERTY()
		int SaveScreenMode;

	UPROPERTY()
		int SaveGeneralQuality;

	UPROPERTY()
		int SaveResolutionScale;

	UPROPERTY()
		int SaveMaterialQScale;

	UPROPERTY()
		int SaveAAQuality;

	UPROPERTY()
		int SaveViewDistance;

	UPROPERTY()
		int SaveShadowQuality;

	UPROPERTY()
		int SaveFoliageQuality;

	UPROPERTY()
		int SaveTexturesQuality;

	UPROPERTY()
		int SaveEffectsQuality;

	UPROPERTY()
		int SavePPQuality;

	UPROPERTY()
		bool bSaveVSync;


};


UCLASS()
class TPS_API UPlayerSettingsDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixMaster;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixAmbient;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixVoise;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixUI;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundMix* MixFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassMaster;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassAmbient;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassVoise;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassUI;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundClass")
		class USoundClass* ClassFX;

};

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, Config = Game)
class TPS_API UPlayerSettingsBase : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public: 



//Audio


	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundMaster = 1.0f;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundAmbient = 1.0f;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundMusic = 1.0f;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundVoise = 1.0f;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundUI = 1.0f;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		float CurrSoundFX = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeSoundMasterVolume(float value);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeSoundMusicVolume(float value);

//Base

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		class UPlayerSettingsDataAsset* SettingsDataAsset;

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void InitDASettings(UPlayerSettingsDataAsset* SettingsDatAsset);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void SaveSettings();

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		bool LoadSettings();

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ApplySettings();

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void SetDefaultSettings();

//Video

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int BaseScreenResolutionX;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int BaseScreenResolutionY;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseScreenMode;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseResolutionScale;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseMaterialQScale;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseAAQuality;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseViewDistance;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseShadowQuality;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseFoliageQuality;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseTexturesQuality;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BaseEffectsQuality;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		bool bBaseVSync;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerSettings|Settings")
		int32 BasePPQuality;

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeVideoResolution(int ResolutionX, int ResolutionY);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeVideoScreenMode(int32 ScreenMode);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeVideoResolutionScale(int32 ResolutionScale);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeMaterialQScale(int32 MaterialScale);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeVideoAntiAliasing(int32 AAQuality);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeViewDistance(int32 ViewDistance);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeShadowQuality(int32 ShadowQuality);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeFoliageQualitye(int32 FoliageQuality);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeTexturesQuality(int32 TexturesQuality);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeEffectsQuality(int32 EffectsQuality);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|Settings")
		void ChangeVSync(bool VSync);


};
