// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPS : ModuleRules
{
	public TPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "Slate", "SlateCore", "UMG", "Niagara" });

		PublicIncludePaths.AddRange(collection: new string[]
		{
			"TPS/Character", "TPS/FuncLibrary", "TPS/Game" , "TPS/UI" , "TPS",
		});
    }
}
